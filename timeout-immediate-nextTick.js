
/**
 * @description Asynchronous C++ API
 */
setImmediate(() => {
    console.log('4. This is immediate.');
});


process.nextTick(() => {
    console.log('2. I am next!!');
});

function logger() {
    console.log('1. I am logger.');
}

/**
 * @description Asynchronous C++ API
 */
setTimeout(() => {
    console.log('3. This is timeout.');
}, 0);

logger();

// - - - - - RESPONSE - - - - - //
/*
    1. I am logger.
    2. I am next!!
    3. This is timeout.
    4. This is immediate.
*/